firewalld:
  enabled: true
  IndividualCalls: 'no'
  LogDenied: 'all'
  AutomaticHelpers: 'system'
  FirewallBackend: 'nftables'
  FlushAllOnReload: 'yes'
  default_zone: public
  
  zones:
    public:
      target: DROP
      rich_rules:
        - family: ipv4
          source:
            # change with your network or load balancer address
            address: 192.168.5.0/24
          port:
            portid: 8000
            protocol: tcp
          accept: true    
    docker:
      description: "zone for docker bridge network interfaces"
      target: ACCEPT
      sources:
        - 172.17.0.1/16

  policies:
    docker-output:
      target: DROP
      description: 'give the container internet access'
      ingress_zone: docker
      egress_zone: ANY
      masquerade: true
      services:
        - http
        - https
        - dns
    docker-forwarding:
      target: ACCEPT
      description: 'allow forwarding to the docker zone'
      ingress_zone: ANY
      egress_zone: docker
      forward_ports:
        - portid: 8000
          protocol: tcp
          to_port: 80
          to_addr: 172.17.0.2
