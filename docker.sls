#!jinja|yaml|gpg
# -*- coding: utf-8 -*-
# vim: ft=yaml
---
docker:
  wanted:
    - docker

  pkg:
    docker:
      use_upstream: repo
      daemon_config:
        iptables: false
        data-root: "/srv/docker"
        bridge: "none"
    compose:
      use_upstream: repo

  networks:
    - frontend

  compose:
    ng:
      shaarli:
        image: 'ghcr.io/shaarli/shaarli:latest'
        container_name: 'shaarli'
        networks:
          - frontend
        ports:
          - '8000:80'
        restart: unless-stopped
        volumes:
          - shaarli-data:/var/www/shaarli/data
          - shaarli-cache:/var/www/shaarli/cache
